// map
export const map = function(func) {
  return function* element() {
    for (const y of value) {
      yield func(y);
    }
  };
};
const add = x => x + 4;

map(add)([10, 20, 30, 40]);
// reduce
const reduce = (fun, value) => {
  const fixed = value;
  return function* element() {
    for (const some of value) {
      yield (fun, value);
    }
  };
};

const multiply = (x, y) => x * y;

console.log(reduce(multiply, 0)([1, 2, 3, 4, 5]));

// filter
const filter = func => {
  return function* element() {
    for (const largest of greater) {
      if (fun(largest)) yield greater;
    }
  };
};

const greater = x => x % 2 !== 0;

filter(greater)([1, 2, 3, 4, 5]);

// max elemnt
function max(value) {
  value.reduce((prev, curr) => {
    if (prev > curr) {
      return prev;
    } else {
      return curr;
    }
  });
}
console.log(max(1, 2, 3, 4));

// Unique
const uniques = (prev, curr) => {
  if (prev.indexOf(curr) === -1) prev.push(curr);
  return prev;
};

const a = array => array.reduce(uniques, []);

console.log(a([1, 2, 3, 4, 5, 5, 4, 3, 2, 1]));

// To Array
let ToArray = function(...args) {
  return args.reduce((prev, curr) => [...prev, curr], []);
};

console.log(ToArray(1, 2, 3, 4, 5, 6));

// pipeline
const pipeline = (...fun) =>
  fun.reduce((fun1, fun2) => (...args) => fun1(fun2(...args)));

const add = x => x + 10;
const mul = x => x * 2;

console.log(pipeline(add, mul)(1, 2, 3, 4, 5, 6));

//compose
const compose = (...fun) =>
  fun.reverse().reduce((fun1, fun2) => (...args) => fun2(fun1(...args)));

const add = x => x + 10;
const mul = x => x * 2;
const array = (...x) => x.reduce((prev, curr) => prev + curr);

console.log(compose(add, mul, array)(1, 2, 3, 4, 5, 6));
